<?php

return [

    // 跳过校验的
    'skip_auth' => [
        'role/shownode' => 1,
        'dictcate/all' => 1,
        'dict/getdictbycateid' => 1,
        'diy/index' => 1,
        'diy/add' => 1,
        'diy/getinfo' => 1,
        'diy/getform' => 1,
        'diy/edit' => 1,
        'diy/del' => 1,
        'flow/getroles' => 1,
        'flow/getusers' => 1,
        'flow/getdept' => 1,
        'flow/gettablefield' => 1,
        'form/allform' => 1,
        'upload/doupload' => 1,
        'upload/img' => 1
    ],
];