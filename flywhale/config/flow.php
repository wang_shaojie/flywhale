<?php
/**
 * Created by PhpStorm.
 * Date: 2022/4/23
 * Time: 18:04
 */

return [

    'flow_status' => [
        1 => '待审批',
        2 => '审批中',
        3 => '已通过',
        4 => '已拒绝',
        5 => '已终止',
        6 => '已挂起',
        7 => '被打回'
    ]
];