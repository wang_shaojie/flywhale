<?php
namespace app\controller;

use app\BaseController;
use flow\FlowEngine;
use flow\providers\FlowProvider;
use think\facade\Db;

class Index extends BaseController
{
    public function index()
    {
        $res = Db::name('dict_cate')->select();
        $map = [];
        foreach ($res as $vo) {
            $map[] = [
                'label' => $vo['name'],
                'value' => $vo['id']
            ];
        }

        return json(['code' => 0, 'data' => $map, 'msg' => 'success']);
    }

    public function hello($name = 'ThinkPHP6')
    {
        $flowModel = new \app\model\Flow();
        $flowInfo = $flowModel->findOne([
            'form_id' => 2
        ])['data'];

        if (empty($flowInfo)) {
            return dataReturn(-1, '流程信息为空');
        }

        $flowEngine = new FlowEngine();
        return json($flowEngine->start($flowInfo, 1, 1, '管理员', [
            'f_xor5un6y7p6f' => '李四',
            'f_5ae5un6y9pjq' => 4500,
            'f_jb35un6yc9q3' => 0,
            'f_2rg5un6yo7oq' => '2022-04-25',
            'flow_operator_id' => 8
        ]));
    }
}
