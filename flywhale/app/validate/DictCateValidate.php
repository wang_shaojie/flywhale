<?php

namespace app\validate;

use think\Validate;

class DictCateValidate extends Validate
{
    protected $rule = [
        'code|编码' => 'require',
        'name|字典名称' => 'require|max:55'
    ];
}