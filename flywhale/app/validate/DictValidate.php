<?php

namespace app\validate;

use think\Validate;

class DictValidate extends Validate
{
    protected $rule = [
        'cate_id|所属分类' => 'require',
        'key|项名称' => 'require|max:55',
        'value|键值' => 'require',
        'status|状态' => 'require'
    ];
}